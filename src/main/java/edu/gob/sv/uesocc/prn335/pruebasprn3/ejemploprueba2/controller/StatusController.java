/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.gob.sv.uesocc.prn335.pruebasprn3.ejemploprueba2.controller;

import edu.gob.sv.uesocc.prn335.pruebasprn3.ejemploprueba2.models.StatuStore;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.enterprise.inject.Stereotype;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;


/**
 *
 * @author cesarlinares
 */
@Stateless
@LocalBean
public class StatusController  {
   
  
    protected  EntityManager EntityMangerContext;
   
/***
 * Method that permit create new rol in  the application 
 * @param statuStore 
 */
    public void Create (StatuStore statuStore){
        if(statuStore!=null){
            EntityMangerContext.persist(statuStore);
            
        }
    }
    /***
     * Method that permite Remove a Rol the appplication 
     * @param statuStore  proporcione the information to elimate 
     */
    public void Delete(StatuStore statuStore){
        
        if(statuStore!=null){
            EntityMangerContext.remove(statuStore);
        }
        
    }
    
    public void Update(StatuStore  statuStore){
        if (statuStore!=null) {
            EntityMangerContext.merge(statuStore);        
        }
    }
    
    public List<StatuStore> StatuStoreListAll(){
      CriteriaQuery SelectcriteryQuery =
                EntityMangerContext.getCriteriaBuilder().createQuery(); 
      
      TypedQuery<StatuStore> query =
      EntityMangerContext.createNamedQuery("StatuStore.findAll", StatuStore.class);
  List<StatuStore> results = query.getResultList();
      
      //        Query  query= EntityMangerContext.createQuery(querySearch);   
      
     SelectcriteryQuery.select(SelectcriteryQuery.from(StatuStore.class));
//        return EntityMangerContext.createQuery(SelectcriteryQuery).getResultList();
return results;
            
         
    }
    
    public List<StatuStore> FindbySearch(String Search){
        
        CriteriaQuery querySearch = EntityMangerContext.getCriteriaBuilder().createQuery();
        querySearch.select(querySearch.from(StatuStore.class)).where((Expression) EntityMangerContext.getReference(StatuStore.class, Search));
        
        
        
//        Query  query= EntityMangerContext.createQuery(querySearch);      
//       query.equals(Search);
        
        return EntityMangerContext.createQuery(querySearch).getResultList();
        
    }
    
    public List<StatuStore> FindbyRange(int start,int end){
        
        CriteriaQuery  queryRange=EntityMangerContext.getCriteriaBuilder().createQuery();
        queryRange.select(queryRange.from(StatuStore.class));
        
      Query  query= EntityMangerContext.createQuery(queryRange);
      query.setMaxResults((end-start)+1);
      query.setFirstResult(start);
      
      return query.getResultList();
                
    }
    
    
    
           
    
 
    
}
