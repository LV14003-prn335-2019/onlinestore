/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.gob.sv.uesocc.prn335.pruebasprn3.ejemploprueba2.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author cesarlinares
 */
@Entity
@Table(name = "CustomerProduct", catalog = "OnlineStore", schema = "")
@NamedQueries({
    @NamedQuery(name = "CustomerProduct.findAll", query = "SELECT c FROM CustomerProduct c"),
    @NamedQuery(name = "CustomerProduct.findByIdCustomerProduct", query = "SELECT c FROM CustomerProduct c WHERE c.idCustomerProduct = :idCustomerProduct"),
    @NamedQuery(name = "CustomerProduct.findByIdcustomer", query = "SELECT c FROM CustomerProduct c WHERE c.idcustomer = :idcustomer"),
    @NamedQuery(name = "CustomerProduct.findByIdProduct", query = "SELECT c FROM CustomerProduct c WHERE c.idProduct = :idProduct")})
public class CustomerProduct implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdCustomerProduct", nullable = false)
    private Integer idCustomerProduct;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Idcustomer", nullable = false)
    private int idcustomer;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdProduct", nullable = false)
    private int idProduct;

    public CustomerProduct() {
    }

    public CustomerProduct(Integer idCustomerProduct) {
        this.idCustomerProduct = idCustomerProduct;
    }

    public CustomerProduct(Integer idCustomerProduct, int idcustomer, int idProduct) {
        this.idCustomerProduct = idCustomerProduct;
        this.idcustomer = idcustomer;
        this.idProduct = idProduct;
    }

    public Integer getIdCustomerProduct() {
        return idCustomerProduct;
    }

    public void setIdCustomerProduct(Integer idCustomerProduct) {
        this.idCustomerProduct = idCustomerProduct;
    }

    public int getIdcustomer() {
        return idcustomer;
    }

    public void setIdcustomer(int idcustomer) {
        this.idcustomer = idcustomer;
    }

    public int getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCustomerProduct != null ? idCustomerProduct.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CustomerProduct)) {
            return false;
        }
        CustomerProduct other = (CustomerProduct) object;
        if ((this.idCustomerProduct == null && other.idCustomerProduct != null) || (this.idCustomerProduct != null && !this.idCustomerProduct.equals(other.idCustomerProduct))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "edu.gob.sv.uesocc.prn335.pruebasprn3.ejemploprueba2.controllers.models.CustomerProduct[ idCustomerProduct=" + idCustomerProduct + " ]";
    }
    
}
