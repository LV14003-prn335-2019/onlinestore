/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.gob.sv.uesocc.prn335.pruebasprn3.ejemploprueba2.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author cesarlinares
 */
@Entity
@Table(name = "Products", catalog = "OnlineStore", schema = "")
@NamedQueries({
    @NamedQuery(name = "Products.findAll", query = "SELECT p FROM Products p"),
    @NamedQuery(name = "Products.findByIdProduct", query = "SELECT p FROM Products p WHERE p.idProduct = :idProduct"),
    @NamedQuery(name = "Products.findByProduct", query = "SELECT p FROM Products p WHERE p.product = :product"),
    @NamedQuery(name = "Products.findBySaleSPrice", query = "SELECT p FROM Products p WHERE p.saleSPrice = :saleSPrice"),
    @NamedQuery(name = "Products.findByBuyPrice", query = "SELECT p FROM Products p WHERE p.buyPrice = :buyPrice"),
    @NamedQuery(name = "Products.findByIdCategory", query = "SELECT p FROM Products p WHERE p.idCategory = :idCategory"),
    @NamedQuery(name = "Products.findByIdProvider", query = "SELECT p FROM Products p WHERE p.idProvider = :idProvider")})
public class Products implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdProduct", nullable = false)
    private Integer idProduct;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "Product", nullable = false, length = 100)
    private String product;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "SaleSPrice", precision = 12)
    private Float saleSPrice;
    @Column(name = "BuyPrice", precision = 12)
    private Float buyPrice;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdCategory", nullable = false)
    private int idCategory;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdProvider", nullable = false)
    private int idProvider;

    public Products() {
    }

    public Products(Integer idProduct) {
        this.idProduct = idProduct;
    }

    public Products(Integer idProduct, String product, int idCategory, int idProvider) {
        this.idProduct = idProduct;
        this.product = product;
        this.idCategory = idCategory;
        this.idProvider = idProvider;
    }

    public Integer getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(Integer idProduct) {
        this.idProduct = idProduct;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public Float getSaleSPrice() {
        return saleSPrice;
    }

    public void setSaleSPrice(Float saleSPrice) {
        this.saleSPrice = saleSPrice;
    }

    public Float getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(Float buyPrice) {
        this.buyPrice = buyPrice;
    }

    public int getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(int idCategory) {
        this.idCategory = idCategory;
    }

    public int getIdProvider() {
        return idProvider;
    }

    public void setIdProvider(int idProvider) {
        this.idProvider = idProvider;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProduct != null ? idProduct.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Products)) {
            return false;
        }
        Products other = (Products) object;
        if ((this.idProduct == null && other.idProduct != null) || (this.idProduct != null && !this.idProduct.equals(other.idProduct))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "edu.gob.sv.uesocc.prn335.pruebasprn3.ejemploprueba2.controllers.models.Products[ idProduct=" + idProduct + " ]";
    }
    
}
