/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.gob.sv.uesocc.prn335.pruebasprn3.ejemploprueba2.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author cesarlinares
 */
@Entity
@Table(name = "Provider", catalog = "OnlineStore", schema = "")
@NamedQueries({
    @NamedQuery(name = "Provider.findAll", query = "SELECT p FROM Provider p"),
    @NamedQuery(name = "Provider.findByIdProvider", query = "SELECT p FROM Provider p WHERE p.idProvider = :idProvider"),
    @NamedQuery(name = "Provider.findByNameProvider", query = "SELECT p FROM Provider p WHERE p.nameProvider = :nameProvider"),
    @NamedQuery(name = "Provider.findByAddress", query = "SELECT p FROM Provider p WHERE p.address = :address"),
    @NamedQuery(name = "Provider.findByCellPhone", query = "SELECT p FROM Provider p WHERE p.cellPhone = :cellPhone")})
public class Provider implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdProvider", nullable = false)
    private Integer idProvider;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NameProvider", nullable = false, length = 100)
    private String nameProvider;
    @Size(max = 200)
    @Column(name = "Address", length = 200)
    private String address;
    @Size(max = 9)
    @Column(name = "cellPhone", length = 9)
    private String cellPhone;

    public Provider() {
    }

    public Provider(Integer idProvider) {
        this.idProvider = idProvider;
    }

    public Provider(Integer idProvider, String nameProvider) {
        this.idProvider = idProvider;
        this.nameProvider = nameProvider;
    }

    public Integer getIdProvider() {
        return idProvider;
    }

    public void setIdProvider(Integer idProvider) {
        this.idProvider = idProvider;
    }

    public String getNameProvider() {
        return nameProvider;
    }

    public void setNameProvider(String nameProvider) {
        this.nameProvider = nameProvider;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProvider != null ? idProvider.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Provider)) {
            return false;
        }
        Provider other = (Provider) object;
        if ((this.idProvider == null && other.idProvider != null) || (this.idProvider != null && !this.idProvider.equals(other.idProvider))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "edu.gob.sv.uesocc.prn335.pruebasprn3.ejemploprueba2.controllers.models.Provider[ idProvider=" + idProvider + " ]";
    }
    
}
