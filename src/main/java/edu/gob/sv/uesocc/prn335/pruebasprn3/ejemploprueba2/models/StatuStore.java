/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.gob.sv.uesocc.prn335.pruebasprn3.ejemploprueba2.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author cesarlinares
 */
 @Entity
@Table(name = "StatuStore", catalog = "OnlineStore", schema = "")
@NamedQueries({
    @NamedQuery(name = "StatuStore.findAll", query = "SELECT s FROM StatuStore s"),
    @NamedQuery(name = "StatuStore.findByIdStatuStore", query = "SELECT s FROM StatuStore s WHERE s.idStatuStore = :idStatuStore"),
    @NamedQuery(name = "StatuStore.findByNameStatus", query = "SELECT s FROM StatuStore s WHERE s.nameStatus = :nameStatus"),
    @NamedQuery(name = "StatuStore.findByDescription", query = "SELECT s FROM StatuStore s WHERE s.description = :description")})
public class StatuStore implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdStatuStore", nullable = false)
    private Integer idStatuStore;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NameStatus", nullable = false, length = 50)
    private String nameStatus;
    @Size(max = 200)
    @Column(name = "Description", length = 200)
    private String description;

    public StatuStore() {
    }

    public StatuStore(Integer idStatuStore) {
        this.idStatuStore = idStatuStore;
    }

    public StatuStore(Integer idStatuStore, String nameStatus) {
        this.idStatuStore = idStatuStore;
        this.nameStatus = nameStatus;
    }

    public Integer getIdStatuStore() {
        return idStatuStore;
    }

    public void setIdStatuStore(Integer idStatuStore) {
        this.idStatuStore = idStatuStore;
    }

    public String getNameStatus() {
        return nameStatus;
    }

    public void setNameStatus(String nameStatus) {
        this.nameStatus = nameStatus;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idStatuStore != null ? idStatuStore.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StatuStore)) {
            return false;
        }
        StatuStore other = (StatuStore) object;
        if ((this.idStatuStore == null && other.idStatuStore != null) || (this.idStatuStore != null && !this.idStatuStore.equals(other.idStatuStore))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "edu.gob.sv.uesocc.prn335.pruebasprn3.ejemploprueba2.controllers.models.StatuStore[ idStatuStore=" + idStatuStore + " ]";
    }
    
}
