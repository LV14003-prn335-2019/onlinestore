/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebacontrollers;

import edu.gob.sv.uesocc.prn335.pruebasprn3.ejemploprueba2.models.StatuStore;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author cesarlinares
 */
@Stateless
public class StatuStoreFacade extends AbstractFacade<StatuStore> implements StatuStoreFacadeLocal {

    @PersistenceContext(unitName = "edu.gob.sv.uesocc.prn335.PruebasPrn3_EjemploPrueba2_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public StatuStoreFacade() {
        super(StatuStore.class);
    }
    
}
