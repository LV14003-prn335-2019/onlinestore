/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebacontrollers;

import edu.gob.sv.uesocc.prn335.pruebasprn3.ejemploprueba2.models.StatuStore;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author cesarlinares
 */
@Local
public interface StatuStoreFacadeLocal {

    void create(StatuStore statuStore);

    void edit(StatuStore statuStore);

    void remove(StatuStore statuStore);

    StatuStore find(Object id);

    List<StatuStore> findAll();

    List<StatuStore> findRange(int[] range);

    int count();
    
}
