/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebacontrollers;

import edu.gob.sv.uesocc.prn335.pruebasprn3.ejemploprueba2.models.StatuStore;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import pruebacontrollers.exceptions.NonexistentEntityException;

/**
 *
 * @author cesarlinares
 */
public class StatuStoreJpaController implements Serializable {

    public StatuStoreJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(StatuStore statuStore) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(statuStore);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(StatuStore statuStore) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            statuStore = em.merge(statuStore);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = statuStore.getIdStatuStore();
                if (findStatuStore(id) == null) {
                    throw new NonexistentEntityException("The statuStore with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            StatuStore statuStore;
            try {
                statuStore = em.getReference(StatuStore.class, id);
                statuStore.getIdStatuStore();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The statuStore with id " + id + " no longer exists.", enfe);
            }
            em.remove(statuStore);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<StatuStore> findStatuStoreEntities() {
        return findStatuStoreEntities(true, -1, -1);
    }

    public List<StatuStore> findStatuStoreEntities(int maxResults, int firstResult) {
        return findStatuStoreEntities(false, maxResults, firstResult);
    }

    private List<StatuStore> findStatuStoreEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(StatuStore.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public StatuStore findStatuStore(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(StatuStore.class, id);
        } finally {
            em.close();
        }
    }

    public int getStatuStoreCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<StatuStore> rt = cq.from(StatuStore.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
