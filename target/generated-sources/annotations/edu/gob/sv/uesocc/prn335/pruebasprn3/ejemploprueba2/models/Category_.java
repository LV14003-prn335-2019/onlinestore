package edu.gob.sv.uesocc.prn335.pruebasprn3.ejemploprueba2.models;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-08-07T16:15:27")
@StaticMetamodel(Category.class)
public class Category_ { 

    public static volatile SingularAttribute<Category, Integer> idStatuStore;
    public static volatile SingularAttribute<Category, String> description;
    public static volatile SingularAttribute<Category, String> category;
    public static volatile SingularAttribute<Category, Integer> idCategory;

}