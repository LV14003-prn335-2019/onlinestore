package edu.gob.sv.uesocc.prn335.pruebasprn3.ejemploprueba2.models;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-08-07T16:15:27")
@StaticMetamodel(Customer.class)
public class Customer_ { 

    public static volatile SingularAttribute<Customer, String> address;
    public static volatile SingularAttribute<Customer, Integer> idStatuStore;
    public static volatile SingularAttribute<Customer, Integer> idcustomer;
    public static volatile SingularAttribute<Customer, String> pass;
    public static volatile SingularAttribute<Customer, String> cellphone;
    public static volatile SingularAttribute<Customer, String> email;
    public static volatile SingularAttribute<Customer, String> username;

}