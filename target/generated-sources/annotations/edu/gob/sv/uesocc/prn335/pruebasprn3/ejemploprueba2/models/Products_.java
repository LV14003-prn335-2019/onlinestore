package edu.gob.sv.uesocc.prn335.pruebasprn3.ejemploprueba2.models;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-08-07T16:15:27")
@StaticMetamodel(Products.class)
public class Products_ { 

    public static volatile SingularAttribute<Products, Float> buyPrice;
    public static volatile SingularAttribute<Products, String> product;
    public static volatile SingularAttribute<Products, Float> saleSPrice;
    public static volatile SingularAttribute<Products, Integer> idProvider;
    public static volatile SingularAttribute<Products, Integer> idProduct;
    public static volatile SingularAttribute<Products, Integer> idCategory;

}