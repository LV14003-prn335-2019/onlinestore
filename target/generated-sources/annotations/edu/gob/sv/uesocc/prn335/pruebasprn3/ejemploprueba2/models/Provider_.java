package edu.gob.sv.uesocc.prn335.pruebasprn3.ejemploprueba2.models;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-08-07T16:15:27")
@StaticMetamodel(Provider.class)
public class Provider_ { 

    public static volatile SingularAttribute<Provider, String> address;
    public static volatile SingularAttribute<Provider, Integer> idProvider;
    public static volatile SingularAttribute<Provider, String> nameProvider;
    public static volatile SingularAttribute<Provider, String> cellPhone;

}